<?php

namespace Hns\Core\Entities;

use BiNet\App\Entities\Entity;
use BiNet\App\Support\Traits\AccessorTrait;

class Post extends Entity {
	use AccessorTrait;

	protected $id;

	private $title;
	private $body;
}