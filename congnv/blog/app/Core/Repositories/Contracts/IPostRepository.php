<?php

namespace Hns\Core\Repositories\Contracts;

use BiNet\App\Repositories\Contracts\IRepository;
use BiNet\App\Support\Pager;
use Hns\Core\Filters\PostFilter;

interface IPostRepository extends IRepository {
	public function all(PostFilter $filter, Pager $pager);
}