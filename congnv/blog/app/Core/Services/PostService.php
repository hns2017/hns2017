<?php

namespace Hns\Core\Services;

use BiNet\App\Support\Pager;
use Hns\Core\Filters\PostFilter;
use Hns\Core\Repositories\Contracts\IPostRepository;

class PostService {
	private $repo;

	public function __construct(IPostRepository $repo) {
		$this->repo = $repo;
	}

	public function all(PostFilter $filter, Pager $pager) {
		return $this->repo->all($filter, $pager);
	}	
}