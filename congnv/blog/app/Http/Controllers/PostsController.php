<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Repositories\PostRepository;
use BiNet\App\Support\Pager;
use Hns\Core\Filters\PostFilter;
use Hns\Core\Services\PostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{
    private $service;

    public function __construct(PostService $service) {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PostFilter $filter, Pager $pager)
    {   
        $items = $this->service->all($filter, $pager);
        dd($items);

        return view('posts.index', compact('items', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'title' => 'required|unique:posts|max:70',
            'content' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $item = new Post;
            $item->title = $request->get('title');
            $item->content = $request->get('content');

            if ($this->repo->save($item)) {
                \Session::flash('status', 'Created new item.');
                return redirect(route('posts.index'));
            }

            \Session::flash('status', 'This action cant be completed now. Pls retry later!');
            return redirect(route('posts.create'))->withInput();
        }

        return redirect(route('posts.create'))->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->repo->findOrFail($id);

        return view('posts.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = $this->repo->findOrFail($id);

        $rules = [
            'title' => 'required|unique:posts|max:70',
            'content' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $item->title = $request->get('title');
            $item->content = $request->get('content');

            if ($this->repo->save($item)) {
                \Session::flash('status', 'Updated post.');
                return redirect(route('posts.index'));
            }

            \Session::flash('status', 'This action cant be completed now. Pls retry later!');
            return redirect(route('posts.edit', $item->id))->withInput();
        }

        return redirect(route('posts.edit', $item->id))->withInput()->withErrors($validator);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->repo->findOrFail($id);
        if ($this->repo->delete($item)) {
            \Session::flash('status', 'Data deleted!');
        } else {
            \Session::flash('status', 'This action cant be completed now. Pls retry later!');
        }

        return redirect(route('posts.index'));
    }
}
