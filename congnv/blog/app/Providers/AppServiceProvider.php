<?php

namespace App\Providers;

use BiNet\App\Factories\Factory;
use Hns\Core\Filters\PostFilter;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindParams();
        $this->registerRepositories();
    }

    public function bindParams() {
        $params = [
            'BiNet\App\Support\Pager', 
            'BiNet\App\Support\Sorter',
            # filters
            'Hns\Core\Filters\PostFilter',
        ];

        $data = request()->all();

        foreach ($params as $param) {
            $this->app->bind($param, function () use ($param, $data) {
                return Factory::createFromArray($param, $data);
            });
        }
    }

    public function registerRepositories() {
        $repos = [
            'PostRepository',
        ];

        foreach ($repos as $repo) {
            $this->app->singleton(
                "Hns\Core\Repositories\Contracts\I$repo",
                "App\Repositories\\$repo"
            );
        }
    }
}
