<?php

namespace App\Repositories;

use App\Models\Post as Model;
use BiNet\App\Factories\Factory;
use BiNet\App\Repositories\Repository;
use BiNet\App\Support\Pager;
use Hns\Core\Entities\Post as Entity;
use Hns\Core\Filters\PostFilter;
use Hns\Core\Repositories\Contracts\IPostRepository;

class PostRepository extends Repository implements IPostRepository {
	protected $model = Model::class;

	public function all(PostFilter $filter, Pager $pager)
	{
		$query = \DB::table('posts');

		if ($filter->id) {
			$query->where('id', $filter->id);
		}

		if ($filter->title) {
			$query->where('title', 'like', "%{$fitler->title}%");
		}
		
		if ($filter->content) {
			$query->where('content', 'like', "%{$filter->content}%");
		}

		$collection = $query->get();

		// entities
		$entities = [];

		foreach ($collection->toArray() as $model) {
			$entities[] = Factory::createFromArray(Entity::class, (array) $model);
		}

		return $entities;
	}
}