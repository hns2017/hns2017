<?php

namespace App\Repositories;
use App\Models\Post;

class PostRepository {
	protected $model = Post::class;

	public function all($id = null, $title = null, $content = null) {
		$query = \DB::table('posts');

		if ($id) {
			$query->where('id', $id);
		}

		if ($title) {
			$query->where('title', 'like', "%$title%");
		}
		
		if ($content) {
			$query->where('content', 'like', "%$content%");
		}

		return $query->get();
	}

	public function save($post) {
		return $post->save();
	}

	public function delete($post) {
		return $post->delete();
	}

	public function __call(string $name, array $args) {
		return call_user_func_array(array($this->model, $name), $args);
	}
}