@extends('layouts.bootstrap')

@section('title', 'Create new user')
@section('content')
<form method="post" action="{{ route('posts.store') }}" class="form-horizontal">
	{{ csrf_field() }}
	<div class="form-group">
		<label for="title">Title:</label>
		<input type="text" name="title" value="{{ old('title') }}" id="title" class="form-control" />
		@if ($errors->has('title'))
		<p class="text-danger">{{ $errors->first('title') }}</p>
		@endif
	</div>
	<div class="form-group">
		<label for="content">Content:</label>
		<textarea name="content" id="content" class="form-control">{{ old('title') }}</textarea> 
		@if ($errors->has('content'))
		<p class="text-danger">{{ $errors->first('content') }}</p>
		@endif
	</div>
	<button type="submit" class="btn btn-default btn-primary">Submit</button>
</form>
@endsection