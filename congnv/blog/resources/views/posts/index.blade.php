@extends('layouts.bootstrap')

@section('title', 'User management')
@section('content')

@if (Session::has('status'))
<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> {{ Session::pull('status') }}
</div>
@endif

<a href="{{ route('posts.create') }}" class="btn btn-success btn-sm">create</a>

<table class="table table-bordered table-striped table-hover">
	<tr>
		<th>id</th>
		<th>title</th>
		<th>content</th>
		<th>actions</th>
	</tr>
	<tr>
		<form method="get" class="form-inline">
			<td><input type="text" name="id" value="{{ $id }}" class="form-control" /></td>
			<td><input type="text" name="title" value="{{ $title }}" class="form-control" /></td>
			<td><input type="text" name="content" value="{{ $content }}" class="form-control" /></td>
			<td><input type="submit" class="btn btn-sm btn-success" value="Search" /></td>
		</form>
	</tr>
	@foreach($items as $item)
	<tr>
		<td>{{ $item->id }}</td>
		<td>{{ $item->title }}</td>
		<td>{{ $item->content }}</td>
		<td>
			<a href="{{ route('posts.edit', ['id' => $item->id]) }}" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-pencil"></i> edit</a>
			<form method="post" action="{{ route('posts.destroy', $item->id) }}">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}
				<button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i> delete</button>
			</form>
		</td>
	</tr>
	@endforeach
</table>
@endsection