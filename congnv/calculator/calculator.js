var inputs = document.getElementsByTagName("input");
for (var i = 0; i < inputs.length; i++) {
	var input = inputs[i];

	input.onclick = function(e) {
		var $this = e.target;
		cal($this.value);	
	};
}

var prevInput = '';

function cal(val) {
	var screen = document.getElementById("screen");

	if (val == "=") {
		screen.value = eval(screen.value);
	} else {
		if (prevInput == "=") {
			screen.value = val;
		} else {
			screen.value += val;
		}
	}

	prevInput = val;
}
