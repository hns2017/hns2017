<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>My Calculator</title>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="style.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>
		<body>
			<h1 class="text-center">Hello Js!</h1>

			<div class="container">
				<div class="col-md-4 col-md-offset-4 text-center">
					<div class="row">
						<textarea id="screen" class="col-sm-12"></textarea>
						<input type="button" value="7" class="btn btn-default col-sm-3" />
						<input type="button" value="8" class="btn btn-default col-sm-3" />
						<input type="button" value="9" class="btn btn-default col-sm-3" />
						<input type="button" value="*" class="btn btn-default col-sm-3" />

						<input type="button" value="4" class="btn btn-default col-sm-3" />
						<input type="button" value="5" class="btn btn-default col-sm-3" />
						<input type="button" value="6" class="btn btn-default col-sm-3" />
						<input type="button" value="-" class="btn btn-default col-sm-3" />

						<input type="button" value="1" class="btn btn-default col-sm-3" />
						<input type="button" value="2" class="btn btn-default col-sm-3" />
						<input type="button" value="3" class="btn btn-default col-sm-3" />
						<input type="button" value="+" class="btn btn-default col-sm-3" />

						<input type="button" value="+-" class="btn btn-default col-sm-3" />
						<input type="button" value="0" class="btn btn-default col-sm-3" />
						<input type="button" value="." class="btn btn-default col-sm-3" />
						<input type="button" value="=" class="btn btn-default col-sm-3" />

					</div>
				</div>
			</div>

			<!-- jQuery -->
			<script src="//code.jquery.com/jquery.js"></script>
			<!-- Bootstrap JavaScript -->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
			<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
			<script src="Hello World"></script>
			<script type="text/javascript" src="calculator.js"></script>
		</body>
		</html>