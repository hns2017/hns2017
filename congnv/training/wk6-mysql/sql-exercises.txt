hanusoft website
==================
sql - insert some records & write sql queries for reports listed below:
Note: you may need to add extra fields to complete these requirements
1. 
	a) list all members. member's name = first name + last name, separated by 1 space.

	ID	Name 			...
	1	Quan Nguyen		...

	b) list top 3 most active members. 
	The member is evaluated as active or not based on the number of projects they joined in.
	c) list first 5 members in group k14 order ascending by members' name
	d) list next 5 members in k14 order ascending by members' name
	e) to optimize db, we use boolean as datatype for Member.gender, male = "Nam", female = "Nu", others = "Khac"
	Display list of users as below
	
	ID	Name 			Gender	...
	1	Quan Nguyen		Male 	...
		
2. 
	a) list all projects, latest -to- oldest ones.
	b) list all projects are on-going
	c) list all members joined in project HanuSoft website
	d) list all projects are implemented by Quann
	e) assign Quan to project HanuSoft website
	
3. 
	a) list all members of Yii team
	b) list all teams (groups) those Quann joined in
	c) fire Quann out of Yii team
	d) (ignore this question) list all projects are implemented by Yii team
	e) report the number of joined in - projects for each member in 2015

Complete these tasks & copy the queries into a .txt file & submit
