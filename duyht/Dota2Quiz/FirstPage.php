<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>First page</title>
    <link rel="stylesheet" href="StyleSheet.css">
</head>
<body>
    <div id="container">
        <div id="banner">
            <img style="width: 100%; height: 270px;" src="Picture/Banner.jpg"/>
        </div>
        <div id="nav_name">
            <h1><b><i>Dota 2 Quiz</i></b></h1>
        </div>
        <form action="SecondPage.php" method="post">
            <div class="quiz_container">
                <p>1. How many heroes are there in Dota 2 until now?</p>
                <input type="radio" name="1" value="A" id="1a" <?php if(isset($_SESSION['FirstPage'][1]) && $_SESSION['FirstPage'][1] == "A") echo 'checked'; ?> ><label for="1a">112</label><br>
                <input type="radio" name="1" value="B" id="1b" <?php if(isset($_SESSION['FirstPage'][1]) && $_SESSION['FirstPage'][1] == "B") echo 'checked'; ?> ><label for="1b">113</label><br>
                <input type="radio" name="1" value="C" id="1c" <?php if(isset($_SESSION['FirstPage'][1]) && $_SESSION['FirstPage'][1] == "C") echo 'checked'; ?> ><label for="1c">114</label><br>
                <input type="radio" name="1" value="D" id="1d" <?php if(isset($_SESSION['FirstPage'][1]) && $_SESSION['FirstPage'][1] == "D") echo 'checked'; ?> ><label for="1d">115</label><br>
            </div>
            <div class="quiz_container">
                <p>2. Which one can be considered as a support hero?</p>
                <input type="radio" name="2" value="A" id="2a" <?php if(isset($_SESSION['FirstPage'][2]) && $_SESSION['FirstPage'][2] == "A") echo 'checked'; ?> ><label for="2a">Shadow Fiend</label><br>
                <input type="radio" name="2" value="B" id="2b" <?php if(isset($_SESSION['FirstPage'][2]) && $_SESSION['FirstPage'][2] == "B") echo 'checked'; ?> ><label for="2b">Anti-mage</label><br>
                <input type="radio" name="2" value="C" id="2c" <?php if(isset($_SESSION['FirstPage'][2]) && $_SESSION['FirstPage'][2] == "C") echo 'checked'; ?> ><label for="2c">Dazzle</label><br>
                <input type="radio" name="2" value="D" id="2d" <?php if(isset($_SESSION['FirstPage'][2]) && $_SESSION['FirstPage'][2] == "D") echo 'checked'; ?> ><label for="2d">Ember spirit</label><br>
            </div>
            <div class="quiz_container">
                <p>3. How many rune spots are there on the map?</p>
                <input type="radio" name="3" value="A" id="3a" <?php if(isset($_SESSION['FirstPage'][3]) && $_SESSION['FirstPage'][3] == "A") echo 'checked'; ?> ><label for="3a">4</label><br>
                <input type="radio" name="3" value="B" id="3b" <?php if(isset($_SESSION['FirstPage'][3]) && $_SESSION['FirstPage'][3] == "B") echo 'checked'; ?> ><label for="3b">5</label><br>
                <input type="radio" name="3" value="C" id="3c" <?php if(isset($_SESSION['FirstPage'][3]) && $_SESSION['FirstPage'][3] == "C") echo 'checked'; ?> ><label for="3c">6</label><br>
                <input type="radio" name="3" value="D" id="3d" <?php if(isset($_SESSION['FirstPage'][3]) && $_SESSION['FirstPage'][3] == "D") echo 'checked'; ?> ><label for="3d">7</label><br>
            </div>
            <div class="quiz_container">
                <p>4. Which hero has the highest base <b>strength</b>?</p>
                <input type="radio" name="4" value="A" id="4a" ><label for="4a">Chaos Knight</label><br>
                <input type="radio" name="4" value="B" id="4b" ><label for="4b">Earth Shaker</label><br>
                <input type="radio" name="4" value="C" id="4c" ><label for="4c">Treant Protector</label><br>
                <input type="radio" name="4" value="D" id="4d" ><label for="4d">Spirit Breaker</label><br>
            </div>
            <div class="quiz_container">
                <p>5. The ultimate <b>Coup de Grace</b> belongs to which hero?</p>
                <input type="radio" name="5" value="A" id="5a" ><label for="5a">Phantom Assassin</label><br>
                <input type="radio" name="5" value="B" id="5b" ><label for="5b">Phantom Lancer</label><br>
                <input type="radio" name="5" value="C" id="5c" ><label for="5c">Rikimaru</label><br>
                <input type="radio" name="5" value="D" id="5d" ><label for="5d">Spectre</label><br>
            </div>
            <div class="quiz_container">
                <p>6. Which of the following skill has the <b>pure</b> damage type?</p>
                <input type="radio" name="6" value="A" id="6a" ><label for="6a">Thundergod Wrath</label><br>
                <input type="radio" name="6" value="B" id="6b" ><label for="6b">Finger of Death</label><br>
                <input type="radio" name="6" value="C" id="6c" ><label for="6c">Counter Helix</label><br>
                <input type="radio" name="6" value="D" id="6d" ><label for="6d">Power Shot</label><br>
            </div>
            <div class="quiz_container">
                <p>7. Which hero doesn't come from DotA?</p>
                <input type="radio" name="7" value="A" id="7a" ><label for="7a">Rubick</label><br>
                <input type="radio" name="7" value="B" id="7b" ><label for="7b">Monkey King</label><br>
                <input type="radio" name="7" value="C" id="7c" ><label for="7c">Twisted Fate</label><br>
                <input type="radio" name="7" value="D" id="7d" ><label for="7d">Brewmaster</label><br>
            </div>
            <div class="quiz_container">
                <p>8. What is the effect of the item <b>Wind lance</b></p>
                <input type="radio" name="8" value="A" id="8a" ><label for="8a">+20 attack speed</label><br>
                <input type="radio" name="8" value="B" id="8b" ><label for="8b">+20 movement speed</label><br>
                <input type="radio" name="8" value="C" id="8c" ><label for="8c">+10 attack speed / +10 movement speed</label><br>
                <input type="radio" name="8" value="D" id="8d" ><label for="8d">+15 attack speed / +15 movement speed</label><br>
            </div>
            <div class="quiz_container">
                <p>9. The recipe <b>Perseverance + Platemail + Energy booster</b> belongs to which item?</p>
                <input type="radio" name="9" value="A" id="9a" ><label for="9a">Lotus Orb</label><br>
                <input type="radio" name="9" value="B" id="9b" ><label for="9b">Eye of Skadi</label><br>
                <input type="radio" name="9" value="C" id="9c" ><label for="9c">Octarine Core</label><br>
                <input type="radio" name="9" value="D" id="9d" ><label for="9d">Radiance</label><br>
            </div>
            <div class="quiz_container">
                <p>10. How many Strength can the hero gain when using <b>Unholy Strength</b> from the item <b>Armlet of Mordiggian</b>?</p>
                <input type="radio" name="10" value="A" id="10a" ><label for="10a">20</label><br>
                <input type="radio" name="10" value="B" id="10b" ><label for="10b">25</label><br>
                <input type="radio" name="10" value="C" id="10c" ><label for="10c">30</label><br>
                <input type="radio" name="10" value="D" id="10d" ><label for="10d">35</label><br>
            </div>
            <div class="quiz_container">
                <p>11. Which item doesn't provide mana regen? </p>
                <input type="radio" name="11" value="A" id="11a" ><label for="11a">Octarine Core</label><br>
                <input type="radio" name="11" value="B" id="11b" ><label for="11b">Ring of Aquila</label><br>
                <input type="radio" name="11" value="C" id="11c" ><label for="11c">Scythe of Vyse</label><br>
                <input type="radio" name="11" value="D" id="11d" ><label for="11d">Battle Fury</label><br>
            </div>
            <div class="quiz_container">
                <p>12. What is the lv 25 talent on the left branch in the talent Tree of Invoker?  </p>
                <input type="radio" name="12" value="A" id="12a" ><label for="12a">-17s Tornado cooldow</label><br>
                <input type="radio" name="12" value="B" id="12b" ><label for="12b">-18s Tornado cooldown</label><br>
                <input type="radio" name="12" value="C" id="12c" ><label for="12c">AOE Deafening Blast</label><br>
                <input type="radio" name="12" value="D" id="12d" ><label for="12d">-17s Deafening Blast</label><br>
            </div>
            <div class="quiz_container">
                <p>13. Which hero has the highest base attack range?</p>
                <input type="radio" name="13" value="A" id="13a" ><label for="13a">Sniper</label><br>
                <input type="radio" name="13" value="B" id="13b" ><label for="13b">Lina</label><br>
                <input type="radio" name="13" value="C" id="13c" ><label for="13c">Crystal Maiden</label><br>
                <input type="radio" name="13" value="D" id="13d" ><label for="13d">Techies</label><br>
            </div>
            <div class="quiz_container">
                <p>14. How many attack damage that Shadow Fiend can gain <b>max</b> from <b>Necromastery?</b></p>
                <input type="radio" name="14" value="A" id="14a" ><label for="14a">72</label><br>
                <input type="radio" name="14" value="B" id="14b" ><label for="14b">92</label><br>
                <input type="radio" name="14" value="C" id="14c" ><label for="14c">144</label><br>
                <input type="radio" name="14" value="D" id="14d" ><label for="14d">184</label><br>
            </div>
            <div class="quiz_container">
                <p>15. How many <b>strength</b> does Centaur Warrunner gain per level? </p>
                <input type="radio" name="15" value="A" id="15a" ><label for="15a">3.9</label><br>
                <input type="radio" name="15" value="B" id="15b" ><label for="15b">4</label><br>
                <input type="radio" name="15" value="C" id="15c" ><label for="15c">4.1</label><br>
                <input type="radio" name="15" value="D" id="15d" ><label for="15d">4.2</label><br>
            </div>
            <div class="quiz_container">
                <p>16. What is the amount of mana drained per cog supposed that the skll <b>Power cogs</b> of Clockwerk is at max level?</p>
                <input type="radio" name="16" value="A" id="16a" ><label for="16a">100</label><br>
                <input type="radio" name="16" value="B" id="16b" ><label for="16b">150</label><br>
                <input type="radio" name="16" value="C" id="16c" ><label for="16c">200</label><br>
                <input type="radio" name="16" value="D" id="16d" ><label for="16d">250</label><br>
            </div>
            <div class="quiz_container">
                <p>17. What is the distance of the skill <b>Gust</b> (Drow Ranger)?</p>
                <input type="radio" name="17" value="A" id="17a" ><label for="17a">800 / 1200 with talent</label><br>
                <input type="radio" name="17" value="B" id="17b" ><label for="17b">900 / 1200 with talent</label><br>
                <input type="radio" name="17" value="C" id="17c" ><label for="17c">800 / 1300 with talent</label><br>
                <input type="radio" name="17" value="D" id="17d" ><label for="17d">900 / 1300 with talent</label><br>
            </div>
            <div class="quiz_container">
                <p>18. How many armor does <b>tower tier 3 has?</b></p>
                <input type="radio" name="18" value="A" id="18a" ><label for="18a">20</label><br>
                <input type="radio" name="18" value="B" id="18b" ><label for="18b">22</label><br>
                <input type="radio" name="18" value="C" id="18c" ><label for="18c">24</label><br>
                <input type="radio" name="18" value="D" id="18d" ><label for="18d">30</label><br>
            </div>
            <div class="quiz_container">
                <p>19. Which hero is not able to cancel channeling skill (base on the hero's skills only)</p>
                <input type="radio" name="19" value="A" id="19a" ><label for="19a">Oracle</label><br>
                <input type="radio" name="19" value="B" id="19b" ><label for="19b">Batrider</label><br>
                <input type="radio" name="19" value="C" id="19c" ><label for="19c">Ember Spirit</label><br>
                <input type="radio" name="19" value="D" id="19d" ><label for="19d">Brewmaster</label><br>
            </div>
            <div class="quiz_container">
                <p>20. Which ultimate skill is not affected if the hero has <b>Aghanim's Scepter</b></p>
                <input type="radio" name="20" value="A" id="20a" ><label for="20a">Chaotic Offering</label><br>
                <input type="radio" name="20" value="B" id="20b" ><label for="20b">Primal Split</label><br>
                <input type="radio" name="20" value="C" id="20c" ><label for="20c">False Promise</label><br>
                <input type="radio" name="20" value="D" id="20d" ><label for="20d">Primal Roar</label><br>
            </div>
            <div class="quiz_container">
                <p>21. What is the amount of <b>Primary Attribute</b> a hero lose when he or she is affected by <b>Whirling Death</b> (Timbersaw)?</p>
                <input type="radio" name="21" value="A" id="21a" ><label for="21a">11%</label><br>
                <input type="radio" name="21" value="B" id="21b" ><label for="21b">12%</label><br>
                <input type="radio" name="21" value="C" id="21c" ><label for="21c">14%</label><br>
                <input type="radio" name="21" value="D" id="21d" ><label for="21d">15%</label><br>
            </div>
            <div class="quiz_container">
                <p>22. What is the cooldown time of the skill <b>Poison Touch</b> (Dazzle) when he has <b>lv25 right branch talent</b> and item <b>Octarine core</b>?</p>
                <input type="radio" name="22" value="A" id="22a" ><label for="22a">1.75s</label><br>
                <input type="radio" name="22" value="B" id="22b" ><label for="22b">1.25s</label><br>
                <input type="radio" name="22" value="C" id="22c" ><label for="22c">0.75s</label><br>
                <input type="radio" name="22" value="D" id="22d" ><label for="22d">0.5s</label><br>
            </div>
            <div class="quiz_container">
                <p>23. What is th maximum number of enemy or neutral creeps that Chen can control (skills, items, talents are included)?</p>
                <input type="radio" name="23" value="A" id="23a" ><label for="23a">5</label><br>
                <input type="radio" name="23" value="B" id="23b" ><label for="23b">6</label><br>
                <input type="radio" name="23" value="C" id="23c" ><label for="23c">7</label><br>
                <input type="radio" name="23" value="D" id="23d" ><label for="23d">8</label><br>
            </div>
            <div class="quiz_container">
                <p>24. What is the combination of elements of the skill <b>Ice Wall</b> (Invoker)</p>
                <input type="radio" name="24" value="A" id="24a" ><label for="24a">Quas Quas Quas</label><br>
                <input type="radio" name="24" value="B" id="24b" ><label for="24b">Quas Quas Wex</label><br>
                <input type="radio" name="24" value="C" id="24c" ><label for="24c">Quas Wex Wex</label><br>
                <input type="radio" name="24" value="D" id="24d" ><label for="24d">Quas Quas Exort</label><br>
            </div>
            <div class="quiz_container">
                <p>25. What is the maximum number of <b>Proximity Mines</b> that Techies can plant?</p>
                <input type="radio" name="25" value="A" id="25a" ><label for="25a">20</label><br>
                <input type="radio" name="25" value="B" id="25b" ><label for="25b">25</label><br>
                <input type="radio" name="25" value="C" id="25c" ><label for="25c">30</label><br>
                <input type="radio" name="25" value="D" id="25d" ><label for="25d">unlimited</label><br>
            </div>
            <br>
            <div class="quiz_container">
                <input type="submit" name="submit" value="Next">
            </div>
        </form>
    </div>
</body>
</html>