<?php session_start();

if (isset($_POST['submit'])) {
    $_SESSION['SecondPage'] = $_POST;
}

$answer = array();
$answer[0] = "0";
$answer[1] = "B";
$answer[2] = "C";
$answer[3] = "C";
$answer[4] = "D";
$answer[5] = "A";
$answer[6] = "C";
$answer[7] = "C";
$answer[8] = "B";
$answer[9] = "A";
$answer[10] = "B";
$answer[11] = "A";
$answer[12] = "B";
$answer[13] = "D";
$answer[14] = "D";
$answer[15] = "B";
$answer[16] = "C";
$answer[17] = "D";
$answer[18] = "B";
$answer[19] = "A";
$answer[20] = "C";
$answer[21] = "D";
$answer[22] = "C";
$answer[23] = "C";
$answer[24] = "D";
$answer[25] = "D";
$answer[26] = "A";
$answer[27] = "C";
$answer[28] = "A";
$answer[29] = "B";
$answer[30] = "C";
$answer[31] = "B";
$answer[32] = "A";
$answer[33] = "D";
$answer[34] = "A";
$answer[35] = "B";
$answer[36] = "C";
$answer[37] = "D";
$answer[38] = "C";
$answer[39] = "A";
$answer[40] = "B";
$answer[41] = "C";
$answer[42] = "B";
$answer[43] = "D";
$answer[44] = "B";
$answer[45] = "C";
$answer[46] = "A";
$answer[47] = "C";
$answer[48] = "A";
$answer[49] = "C";
$answer[50] = "A";

$correct_num = 0;


//Check page 1

for ($i = 1; $i <= 25; $i++) {
    if(isset($_SESSION['FirstPage'][$i])){
        if ($_SESSION['FirstPage'][$i] == $answer[$i]) {
            $correct_num++;
        }
    }
}
//Check page 2
for ($i = 26; $i <= 50; $i++) {
    if(isset($_SESSION['SecondPage'][$i])) {
        if ($_SESSION['SecondPage'][$i] == $answer[$i]) {
            $correct_num++;
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>First page</title>
    <link rel="stylesheet" href="StyleSheet.css">
</head>
<body>
<div id="rs_container">
    <div id="banner">
        <img style="width: 100%; height: 270px;" src="Picture/Banner.jpg"/>
    </div>
    <div id="rs_nav_name">
        <h1><b><i>Dota 2 Quiz</i></b></h1>
    </div>
    <div id="result">
        <p><b><i>Your result</i></b>: <?= $correct_num; ?>/50</p>
        <?php
            if($correct_num <= 15){
                echo "You're so bad! Better quit the game!";
            }
            else if ($correct_num > 15 && $correct_num <= 40){
                echo "Pretty good! Keep playing the game!";
            }
            else{
                echo "Your knowledge about the game is very good!";
            }
        ?>
    </div>
</div>
<?php
session_unset();
?>
</body>
</html>