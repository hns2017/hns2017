<?php session_start();
    if(isset($_POST['submit'])){
        $_SESSION['FirstPage'] = $_POST;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Second Page</title>
    <link rel="stylesheet" href="StyleSheet.css">
</head>
<body>
<div id="container">
    <div id="banner">
        <img style="width: 100%; height: 270px;" src="Picture/Banner.jpg"/>
    </div>
    <div id="nav_name">
        <h1><b><i>Dota 2 Quiz</i></b></h1>
    </div>
    <form action="Result.php" method="post">
        <div class="quiz_container">
            <p>26. What is the name of the biggest DotA 2 event?</p>
            <input type="radio" name="26" value="A" id="26a" ><label for="26a">The International</label><br>
            <input type="radio" name="26" value="B" id="26b" ><label for="26b">Dota 2 Major</label><br>
            <input type="radio" name="26" value="C" id="26c" ><label for="26c">Starladder</label><br>
            <input type="radio" name="26" value="D" id="26d" ><label for="26d">World Championship</label><br>
        </div>
        <div class="quiz_container">
            <p>27. Who is the first player to have <b>8000</b> MMR in the world?</p>
            <input type="radio" name="27" value="A" id="27a" ><label for="27a">Miracle-</label><br>
            <input type="radio" name="27" value="B" id="27b" ><label for="27b">Dendi</label><br>
            <input type="radio" name="27" value="C" id="27c" ><label for="27c">w33</label><br>
            <input type="radio" name="27" value="D" id="27d" ><label for="27d">Forev</label><br>
        </div>
        <div class="quiz_container">
            <p>28. Who is the first player to have <b>9000 MMR</b> in the world?</p>
            <input type="radio" name="28" value="A" id="28a" ><label for="28a">Miracle-</label><br>
            <input type="radio" name="28" value="B" id="28b" ><label for="28b">Dendi</label><br>
            <input type="radio" name="28" value="C" id="28c" ><label for="28c">w33</label><br>
            <input type="radio" name="28" value="D" id="28d" ><label for="28d">Forev</label><br>
        </div>
        <div class="quiz_container">
            <p>29. Which team won the first <b>International Dota2</b>?</p>
            <input type="radio" name="29" value="A" id="29a" ><label for="29a">Invictus Gaming (IG)</label><br>
            <input type="radio" name="29" value="B" id="29b" ><label for="29b">Natus Vincere (Navi)</label><br>
            <input type="radio" name="29" value="C" id="29c" ><label for="29c">The Alliance (A)</label><br>
            <input type="radio" name="29" value="D" id="29d" ><label for="29d">Evil Geniuses (EG)</label><br>
        </div>
        <div class="quiz_container">
            <p>30. Which player never win <b>The International?</b></p>
            <input type="radio" name="30" value="A" id="30a" ><label for="30a">Loda</label><br>
            <input type="radio" name="30" value="B" id="30b" ><label for="30b">Fear</label><br>
            <input type="radio" name="30" value="C" id="30c" ><label for="30c">Arteezy</label><br>
            <input type="radio" name="30" value="D" id="30d" ><label for="30d">Iceice</label><br>
        </div>
        <div class="quiz_container">
            <p>31. What is "<b>Sumail's</b> hometown?</p>
            <input type="radio" name="31" value="A" id="31a" ><label for="31a">Iraq</label><br>
            <input type="radio" name="31" value="B" id="31b" ><label for="31b">Israel</label><br>
            <input type="radio" name="31" value="C" id="31c" ><label for="31c">Pakistan</label><br>
            <input type="radio" name="31" value="D" id="31d" ><label for="31d">Russia</label><br>
        </div>
        <div class="quiz_container">
            <p>32. How many games has been played in total in all Grand Finals of <b>The International</b> until now?</p>
            <input type="radio" name="32" value="A" id="32a" ><label for="32a">24</label><br>
            <input type="radio" name="32" value="B" id="32b" ><label for="32b">25</label><br>
            <input type="radio" name="32" value="C" id="32c" ><label for="32c">26</label><br>
            <input type="radio" name="32" value="D" id="32d" ><label for="32d">27</label><br>
        </div>
        <div class="quiz_container">
            <p>33. Which team is famous for the strategy <b>Rat Dota</b></p>
            <input type="radio" name="33" value="A" id="33a" ><label for="33a">Natus Vincere</label><br>
            <input type="radio" name="33" value="B" id="33b" ><label for="33b">Fnatic</label><br>
            <input type="radio" name="33" value="C" id="33c" ><label for="33c">Virtus Pro</label><br>
            <input type="radio" name="33" value="D" id="33d" ><label for="33d">The Alliance</label><br>
        </div>
        <div class="quiz_container">
            <p>34. The <b>Foutain Hook</b> combo became famous due to the play of which two players?</p>
            <input type="radio" name="34" value="A" id="34a" ><label for="34a">Puppey + Dendi</label><br>
            <input type="radio" name="34" value="B" id="34b" ><label for="34b">Loda + Akke</label><br>
            <input type="radio" name="34" value="C" id="34c" ><label for="34c">PPD + Fear</label><br>
            <input type="radio" name="34" value="D" id="34d" ><label for="34d">Notail + Miracle-</label><br>
        </div>
        <div class="quiz_container">
            <p>35. Who is the first carry player of team <b>Secret</b>?</p>
            <input type="radio" name="35" value="A" id="35a" ><label for="35a">Arteezy</label><br>
            <input type="radio" name="35" value="B" id="35b" ><label for="35b">Kuroky</label><br>
            <input type="radio" name="35" value="C" id="35c" ><label for="35c">Fly</label><br>
            <input type="radio" name="35" value="D" id="35d" ><label for="35d">S4</label><br>
        </div>
        <div class="quiz_container">
            <p>36. The phrase <b>1 million Dream Coil</b> refers to the play of which player?</p>
            <input type="radio" name="36" value="A" id="36a" ><label for="36a">Dendi</label><br>
            <input type="radio" name="36" value="B" id="36b" ><label for="36b">Mushi</label><br>
            <input type="radio" name="36" value="C" id="36c" ><label for="36c">S4</label><br>
            <input type="radio" name="36" value="D" id="36d" ><label for="36d">Loda</label><br>
        </div>
        <div class="quiz_container">
            <p>37. In the <b>Solo Mid Champion</b> event in TI3, <b>Iceiceice</b> and <b>Mushi</b> played which hero in the last game of the fianl match?</p>
            <input type="radio" name="37" value="A" id="37a" ><label for="37a">Puck</label><br>
            <input type="radio" name="37" value="B" id="37b" ><label for="37b">Queen of Pain</label><br>
            <input type="radio" name="37" value="C" id="37c" ><label for="37c">Pudge</label><br>
            <input type="radio" name="37" value="D" id="37d" ><label for="37d">Timbersaw</label><br>
        </div>
        <div class="quiz_container">
            <p>38. Which player is famous for denying the <b>Aegis of immortal</b>?</p>
            <input type="radio" name="38" value="A" id="38a" ><label for="38a">Net</label><br>
            <input type="radio" name="38" value="B" id="38b" ><label for="38b">Mushi</label><br>
            <input type="radio" name="38" value="C" id="38c" ><label for="38c">Kyxy</label><br>
            <input type="radio" name="38" value="D" id="38d" ><label for="38d">Hyhy</label><br>
        </div>
        <div class="quiz_container">
            <p>39. How many players are there have won 2 TI?</p>
            <input type="radio" name="39" value="A" id="39a" ><label for="39a">0</label><br>
            <input type="radio" name="39" value="B" id="39b" ><label for="39b">1</label><br>
            <input type="radio" name="39" value="C" id="39c" ><label for="39c">2</label><br>
            <input type="radio" name="39" value="D" id="39d" ><label for="39d">3</label><br>
        </div>
        <div class="quiz_container">
            <p>40. Which hero was never picked in <b>The International 3</b>?</p>
            <input type="radio" name="40" value="A" id="40a" ><label for="40a">Ogre Magi</label><br>
            <input type="radio" name="40" value="B" id="40b" ><label for="40b">Brewmaster</label><br>
            <input type="radio" name="40" value="C" id="40c" ><label for="40c">Zues</label><br>
            <input type="radio" name="40" value="D" id="40d" ><label for="40d">Bloodseeker</label><br>
        </div>
        <div class="quiz_container">
            <p>41. Team <b>DK</b> was eliminated by which team in <b>The International 4</b>?</p>
            <input type="radio" name="41" value="A" id="41a" ><label for="41a">Newbee</label><br>
            <input type="radio" name="41" value="B" id="41b" ><label for="41b">LGD Gaming</label><br>
            <input type="radio" name="41" value="C" id="41c" ><label for="41c">Vici Gaming</label><br>
            <input type="radio" name="41" value="D" id="41d" ><label for="41d">Cloud9</label><br>
        </div>
        <div class="quiz_container">
            <p>42. Which team was invited to <b>The International 1</b> but didn't join the tournament?</p>
            <input type="radio" name="42" value="A" id="42a" ><label for="42a">MiTH.Trust</label><br>
            <input type="radio" name="42" value="B" id="42b" ><label for="42b">StarsBoba</label><br>
            <input type="radio" name="42" value="C" id="42c" ><label for="42c">MUFC</label><br>
            <input type="radio" name="42" value="D" id="42d" ><label for="42d">Meet Your Makers</label><br>
        </div>
        <div class="quiz_container">
            <p>43. Team <b>OG</b> defeated which team in the Grand Final of Frankfurt Major 2015?</p>
            <input type="radio" name="43" value="A" id="43a" ><label for="43a">The Alliance</label><br>
            <input type="radio" name="43" value="B" id="43b" ><label for="43b">Natus Vincere</label><br>
            <input type="radio" name="43" value="C" id="43c" ><label for="43c">Evil Geniuses</label><br>
            <input type="radio" name="43" value="D" id="43d" ><label for="43d">Team Secret</label><br>
        </div>
        <div class="quiz_container">
            <p>44. Which of these players is famous for the carry position in the past?</p>
            <input type="radio" name="44" value="A" id="44a" ><label for="44a">Yaphets</label><br>
            <input type="radio" name="44" value="B" id="44b" ><label for="44b">ZSMJ</label><br>
            <input type="radio" name="44" value="C" id="44c" ><label for="44c">Trixi</label><br>
            <input type="radio" name="44" value="D" id="44d" ><label for="44d">Hyhy</label><br>
        </div>
        <div class="quiz_container">
            <p>45. Which player used to use the in-game name <b>BigDaddy</b>?</p>
            <input type="radio" name="45" value="A" id="45a" ><label for="45a">Fly</label><br>
            <input type="radio" name="45" value="B" id="45b" ><label for="45b">Era</label><br>
            <input type="radio" name="45" value="C" id="45c" ><label for="45c">Notail</label><br>
            <input type="radio" name="45" value="D" id="45d" ><label for="45d">H4nn1</label><br>
        </div>
        <div class="quiz_container">
            <p>46. Which player has never played in the competitive scene?</p>
            <input type="radio" name="46" value="A" id="46a" ><label for="46a">Attacker</label><br>
            <input type="radio" name="46" value="B" id="46b" ><label for="46b">Admiral Bulldog</label><br>
            <input type="radio" name="46" value="C" id="46c" ><label for="46c">XBOCT</label><br>
            <input type="radio" name="46" value="D" id="46d" ><label for="46d">mason</label><br>
        </div>
        <div class="quiz_container">
            <p>47. When was the <b>Compedium Book</b> first appeared?</p>
            <input type="radio" name="47" value="A" id="47a" ><label for="47a">2011</label><br>
            <input type="radio" name="47" value="B" id="47b" ><label for="47b">2012</label><br>
            <input type="radio" name="47" value="C" id="47c" ><label for="47c">2013</label><br>
            <input type="radio" name="47" value="D" id="47d" ><label for="47d">2014</label><br>
        </div>
        <div class="quiz_container">
            <p>48. Where was <b>The International 1</b> held?</p>
            <input type="radio" name="48" value="A" id="48a" ><label for="48a">Germany</label><br>
            <input type="radio" name="48" value="B" id="48b" ><label for="48b">America</label><br>
            <input type="radio" name="48" value="C" id="48c" ><label for="48c">China</label><br>
            <input type="radio" name="48" value="D" id="48d" ><label for="48d">Russia</label><br>
        </div>
        <div class="quiz_container">
            <p>49. The documentary <b>Free to play</b> is about 3 players: Dendi, Fear and ____ ?</p>
            <input type="radio" name="49" value="A" id="49a" ><label for="49a">Kyxy</label><br>
            <input type="radio" name="49" value="B" id="49b" ><label for="49b">Loda</label><br>
            <input type="radio" name="49" value="C" id="49c" ><label for="49c">Hyhy</label><br>
            <input type="radio" name="49" value="D" id="49d" ><label for="49d">Artstyle</label><br>
        </div>
        <div class="quiz_container">
            <p>50. Which team was directly invited to <b>The International 5</b>?</p>
            <input type="radio" name="50" value="A" id="50a" ><label for="50a">Fnatic</label><br>
            <input type="radio" name="50" value="B" id="50b" ><label for="50b">Natus Vincere</label><br>
            <input type="radio" name="50" value="C" id="50c" ><label for="50c">EHOME</label><br>
            <input type="radio" name="50" value="D" id="50d" ><label for="50d">CDEC</label><br>
        </div>
        <br>
        <div class="quiz_container">
            <input type="submit" name="submit" value="Submit">
        </div>
    </form>
</div>
</body>
</html>