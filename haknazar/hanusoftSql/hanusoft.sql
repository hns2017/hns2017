CREATE DATABASE hanusoft;
USE hanusoft;

CREATE TABLE users(
    id int(11) unsigned NOT NULL AUTO_INCREMENT,
    username varchar(50) NOT NULL UNIQUE,
    password varchar(61) NOT NULL,
    role int(1) NOT NULL,
	firstname varchar(50) NOT NULL,
	lastname varchar(50) NOT NULL,
	email varchar(254) NOT NULL UNIQUE,
	gender enum('male','female','others') DEFAULT 'male',
	avatar varchar(255) ,
    dob date,
	description MEDIUMTEXT,
	PRIMARY KEY (id)
); 


CREATE TABLE projects(
    id int(4) unsigned NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL UNIQUE,
	description MEDIUMTEXT ,
	source_code varchar(255),
	status enum ('ongoing','suspended','finished') NOT NULL DEFAULT 'ongoing',
	PRIMARY KEY (id)
);

CREATE TABLE user_projects(
    id int NOT NULL AUTO_INCREMENT,
    project_id int(4) unsigned NOT NULL,
	user_id int(11) unsigned NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (project_id) REFERENCES projects(id),
	FOREIGN KEY (user_id) REFERENCES users(id)
	
);



CREATE TABLE IF NOT EXISTS `groups` (
`id` int(4) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `group_assignments` (
`id` int(11) unsigned NOT NULL,
  `group_id` int(4) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE group_assignments
ADD CONSTRAINT
 FOREIGN KEY (group_id) REFERENCES groups (id);
ALTER TABLE group_assignments
ADD CONSTRAINT
 FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
