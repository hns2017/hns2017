<?php
 $host = 'localhost';
 $username = 'root';
 $password = '';
 $dbname = 'hanuclub';

 try {
 	$conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
 	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 }
 catch(PDOException $e)
 {
 	die ("Connection failed: " . $e->getMessage());
 }
?> 