<?php 
include('includes/connect.php'); 
include('repositories/UserRepository.php');
include('repositories/GroupRepository.php');

/* get logged in user */
$loggedInUserID = 100;
$loggedInUser = getUserByID($loggedInUserID, $conn);

/* get all users */
$users = getAllUsers($conn);

/* demo update/insert/delete */
$sql = "UPDATE user SET email='Hoa Dai Nhan' WHERE id=100";
$result = $conn->query($sql);

if ($result) {
	$msg = "success";
} else {
	$msg = "fail";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Bootstrap 101 Template</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>
  <body>
  	<h1>Hello, <?php echo $loggedInUser['username']; ?>!</h1>

  	<div class="container">
  		<label class="alert alert-success"><?php echo $msg; ?></label>
  		<table class="table table-bordered table-striped">
  			<thead>
  				<tr>
  					<th>ID</th>
  					<th>Username</th>
  				</tr>
  			</thead>
  			<tbody>
  			<?php foreach ($users as $user): ?>
  				<tr>
  					<td><?php echo $user['id']; ?></td>
  					<td><?php echo $user['username']; ?></td>
  				</tr>
  			<?php endforeach; ?>
  			</tbody>
  		</table>
  	</div>

  	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  	<!-- Include all compiled plugins (below), or include individual files as needed -->
  	<script src="js/bootstrap.min.js"></script>
  </body>
  </html>