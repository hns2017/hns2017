<?php 
/**
 * returns user record specified by id or null if not exists
 */
function getUserByID($id, $conn) {
	$sql = "SELECT concat(firstname, ' ', lastname) as name FROM users WHERE id=$id";
	$result = $conn->query($sql);
	return $result->fetch();
}	

/**
 * returns array of user records
 * e.g: ...
 */
function getAllUser($conn) {
	$sql = "SELECT * FROM users";
	$result = $conn->query($sql);
	return $result->fetchAll();
}
function getAvatarByID($id, $conn){
	$sql = "SELECT avatar FROM users WHERE id = .$id";
	$result = $conn->query($sql);
	return $result->fetch();
}
function getDescriptionByID($id, $conn){
$sql = "SELECT description FROM users WHERE id = .$id";
	$result = $conn->query($sql);
	return $result->fetch();
}
function getNumberOfUser($conn) {
	$sql = "SELECT count(id) FROM users";
	$result = $conn->query($sql);
	return $result->fetchAll();
}