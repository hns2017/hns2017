<!DOCTYPE html>
<html>
<head>
	<title>Menu</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
	<link rel="stylesheet" type="text/css" href="myStyle.css">
	<style type="text/css">
	body {
		margin-bottom: 10%;
		text-align: center;
	}
	</style>
</head>
<body>
	
	<form name ="Calculator">
		<input name="display" placeholder="0" style="text-align: right" />
		<br>
		<input type ="button" value = "1" onClick= "document.Calculator.display.value += '1'"/>
		<input type ="button" value = "2" onClick= "document.Calculator.display.value += '2'"/>
		<input type ="button" value = "3" onClick= "document.Calculator.display.value += '3'"/>
		<br>
		<input type ="button" value = "4" onClick= "document.Calculator.display.value += '4'"/>
		<input type ="button" value = "5" onClick= "document.Calculator.display.value += '5'"/>
		<input type ="button" value = "6" onClick= "document.Calculator.display.value += '6'"/>
		<br>
		<input type ="button" value = "7" onClick= "document.Calculator.display.value += '7'"/>
		<input type ="button" value = "8" onClick= "document.Calculator.display.value += '8'"/>
		<input type ="button" value = "9" onClick= "document.Calculator.display.value += '9'"/>
		<br>
		<input type ="button" value = "x" onclick="multiple()"/>
		<input type ="button" value = ":" onclick="div()"/>
		<input type ="button" value = "+" onclick="plus()">
		<input type ="button" value = "-" onclick="minor()"/>
		<input type ="button" value = "=" onclick= "document.Calculator.display.value = eval(document.Calculator.display.value)"/>
		<input type ="button" value = "." onclick="dot()"/>
	</form>
	<script type="text/javascript">
		function multiple()
		{
			document.Calculator.display.value += "x";
		document.Calculator.display.style.textAlign="right";
	}
	function div()
		{
			document.Calculator.display.value += "/";
		document.Calculator.display.style.textAlign="right";
	}
	function plus()
		{
			document.Calculator.display.value += "+";
		document.Calculator.display.style.textAlign="right";
	}
	function minor()
		{
			document.Calculator.display.value += "-";
		document.Calculator.display.style.textAlign="right";
	}
	function dot()
		{
			document.Calculator.display.value += ".";
		document.Calculator.display.style.textAlign="right";
	}
</script>
</body>
</html>