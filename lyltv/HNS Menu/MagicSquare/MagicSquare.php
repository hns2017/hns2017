<?php
if (isset($_POST["number"])) {
	$number = $_POST["number"];
	$a = array(0,$number/2 - 0.5);
	for ($i=1; $i <= $number*$number ; $i++) {
		if ($i == 1) {
			$a = array(0,$number/2-0.5);
		}
		elseif ($i > 1) {
			$b = nextPos($a, $number, $array);
			$a = $b;
		}
		$array[$a[0]][$a[1]] = $i;
	}
}
function nextPos($a, $number, $array)
{ $arr[0] = $a[0];
	$arr[1] = $a[1];
	if ($a[0]-1>=0 && $a[1] + 1 < $number && !(isset($array[$a[0]-1][$a[1]+1])))
	{
		$arr[0] = $a[0]-1;
		$arr[1] = $a[1]+1;
		 //normal case
		
	}
	elseif ($a[0]-1>=0 && $a[1] + 1 < $number && isset($array[$a[0]-1][$a[1]+1])){
	 // isset
		$arr[0] = $a[0]+1;
		$arr[1] = $a[1];
	}
	elseif ($a[1] + 1 == $number && $a[0] == 0){
		//right + up
		$arr[0] = 1;
		$arr[1] = $a[1];
		
	}
	elseif ($a[1] + 1 == $number && $a[0] != 0) {
		//right and not up
		$arr[0] = $a[0] - 1;
		$arr[1] = 0;
		
	}
	elseif ($a[0] == 0 && $a[1] < $number -1){
		$arr[0] = $number-1 ;
		$arr[1] = $a[1] + 1;
		
	}
	return $arr;
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Magic Square</title>
	<link rel="stylesheet" type="text/css" href="myStyle.css">
	<style type="text/css">
		h1 {
			margin-top:3.5%;
			margin-bottom: 0.1%;
			font-size: 250%;
		}
		h2 {
			margin-top:0.2%;
			font-size: 195%;
		}
	</style>
</head>
<body>
	<div id = "cover">
		<h1 class="h"> Enter the number you want, and I'll show you the magic!</h1>
		<h2 class="h"> Remember, just for odd number!</h2>

		<form action = "MagicSquare.php" method = "POST">
			<div><input type="text" name="number"></div>
			<div><input type="submit" value = "Get result" style="margin-bottom: 1%"></div>
		</form>

		<?php 
		if (isset($_POST["number"])) {
			?>
			<div id = "tab"><table align="center">
				<?php 
				for ($i=0; $i < $number ; $i++) {
					?>
					<tr>
						<?php
						for ($j=0; $j < $number ; $j++) { 
							?>
							<td> <?php echo $array[$i][$j] ?> </td>
							<?php
						}
						?>
					</tr>
					<?php
				}
				?>
			</table>
			<?php
		}
		?>
	</div>
	<div style="position:relative;bottom:8%;width:100%;text-align: center;">
		<a href= "../FirstPage.php">Back to home page</a>
	</div>
</div>
</body>
</html>