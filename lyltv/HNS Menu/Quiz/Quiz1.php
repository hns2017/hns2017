<?php
session_start();
if ($_POST) {
	for ($i = 1; $i < 26; $i++) {
		if (isset($_POST["question$i"])) {
			$_SESSION["question$i"] = $_POST["question$i"];
		} else {
			$_SESSION["question$i"] = null;
		}
	}
	header('Location: Quiz2.php');
	die();
}
?> 
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Lily Home</title>
    <link rel="stylesheet" type="text/css" href="myStyle.css">
</head>
<body>
    <div class = "cover">
        <div>
            <h3 style = "text-align:center;"> DBS QUIZ </h3 >
                <form action = "Quiz1.php" method = "POST">
                    <?php
                    $myfile = file('questionBank1.txt');
                    for ($i = 0; $i < 25; $i++) {
                        $id = $i + 1;
                        ?>
                        <h3> Question <?php echo $id ?> </h3>
                        <div class = "question">
                            <p> <?= $myfile[$i * 5] ?> </p>
                            <div>
                                <label><input type="radio" name="question<?= $id ?>" value="A" <?php if (isset($_SESSION["question$id"]) && $_SESSION["question$id"] == "A") echo 'checked'; ?> > <?= $myfile[$i * 5 + 1] ?></label><br>
                            </div>
                            <div>
                                <label><input type="radio" name="question<?= $id ?>" value="B" <?php if (isset($_SESSION["question$id"]) && $_SESSION["question$id"] == "B") echo 'checked'; ?> > <?= $myfile[$i * 5 + 2] ?></label><br>
                            </div>
                            <div>
                                <label><input type="radio" name="question<?= $id ?>" value="C" <?php if (isset($_SESSION["question$id"]) && $_SESSION["question$id"] == "C") echo 'checked'; ?> > <?= $myfile[$i * 5 + 3] ?></label><br>
                            </div>
                            <div>
                                <label><input type="radio" name="question<?= $id ?>" value="D" <?php if (isset($_SESSION["question$id"]) && $_SESSION["question$id"] == "D") echo 'checked'; ?> > <?= $myfile[$i * 5 + 4] ?></label><br>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div><input type="submit" value = "Next page"></div>
                </form>
            </div>
            <div style="position:relative;bottom:10px;width:100%;text-align: center;">
                <a href= "../FirstPage.php">Back to home page</a>s
            </div>
        </div>
    </body>
    </html>