<?php
session_start();
if ($_POST) {
    for ($i = 26; $i < 51; $i++) {
        if (isset($_POST["question$i"])) {
            $_SESSION["question$i"] = $_POST["question$i"];
        } else {
            $_SESSION["question$i"] = null;
        }
    }
	if (isset($_POST['redir'])) {
		$redir = $_POST['redir'];
	}
	if ($redir == 'Lily') {
		header('Location: Quiz1.php');
	} else {
		header('Location: Quiz3.php');
	}
	die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Lily2</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="myStyle.css">
    </head>
    <body>
	<div class="cover">
		<?php
		for ($i = 1; $i < 51; $i++) {
			if (isset($_SESSION["question$i"])) {
			//	echo "question$i = " . $_SESSION["question$i"] . "<br />";
			} else {
			//	echo "Question $i is not answered.<br />";
			}
		}
		?>

        <h3 style = "text-align:center;"> DBS QUIZ </h3 >
        <form id="myform" action = "Quiz2.php" method = "POST">
            <?php
            $myfile = file('questionBank1.txt') or die("Unable to open file!");
            for ($i = 25; $i < 50; $i++) {
                $id = $i + 1;
                ?>
                <h3> Question <?php echo $id ?> </h3>
                <div>
                    <p> <?= $myfile[$i * 5] ?> </p>
                    <div>
                        <label><input type="radio" name="question<?= $id ?>" value="A" <?php if (isset($_SESSION["question$id"]) && $_SESSION["question$id"] == "A") echo 'checked'; ?> > <?= $myfile[$i * 5 + 1] ?></label><br>
                    </div>
                    <div>
                        <label><input type="radio" name="question<?= $id ?>" value="B" <?php if (isset($_SESSION["question$id"]) && $_SESSION["question$id"] == "B") echo 'checked'; ?> > <?= $myfile[$i * 5 + 2] ?></label><br>
                    </div>
                    <div>
                        <label><input type="radio" name="question<?= $id ?>" value="C" <?php if (isset($_SESSION["question$id"]) && $_SESSION["question$id"] == "C") echo 'checked'; ?> > <?= $myfile[$i * 5 + 3] ?></label><br>
                    </div>
                    <div>
                        <label><input type="radio" name="question<?= $id ?>" value="D" <?php if (isset($_SESSION["question$id"]) && $_SESSION["question$id"] == "D") echo 'checked'; ?> > <?= $myfile[$i * 5 + 4] ?></label><br>
                    </div>
                    <?php
                }
				?>
				<input type="hidden" id="redirInput" name="redir" value="action" />
				<div><input type="submit" value = "SUBMIT"></div>
				<a href="#" onclick="document.getElementById('redirInput').value='Lily';document.getElementById('myform').submit();return false;">BACK</a>
		</form>
        <div style="position:relative;bottom:10px;width:100%;text-align: center;">
            <a href= "../FirstPage.php">Back to home page</a>
        </div>
    </div>
</body>
</html>