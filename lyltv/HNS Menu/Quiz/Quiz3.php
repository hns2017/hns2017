<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> SUBMIT </title>
    <link rel="stylesheet" type="text/css" href="myStyle.css">
</head>
<body>
<div>
    <div class = "cover">
        <?php
        $myfile = fopen('solution.txt', "r") or die("Unable to open file!");
        for ($i = 1; $i < 51; $i++) {
            $select[$i] = $_SESSION["question$i"];
				// echo "question $i = " . $select[$i] . "<br />";
            $solution[$i] = fgets($myfile);
            $solution[$i] = substr($solution[$i], 0, 1);
        }

        $score = 0;
        for ($i = 1; $i < 51; $i++) {
            if ($select[$i] == $solution[$i]) {
                $score++;
            }
        }
        echo $score;
        session_destroy();
        ?>
    </div>
    <div style="position:absolute;bottom:10px;width:100%;text-align: center; color: black;">
        <a href= "../FirstPage.php">Back to home page</a>
    </div>
</div>
</body>
</html>	