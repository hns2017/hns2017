<section id="about" class="about-section">
    <div class="row">

        <div class="col-md-6 col-sm-12 col-xs-12">
            <img class="img-responsive" src="../rolling/images/bg/2.jpg" draggable="false" alt="">
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="about-me section-space-padding">

                <h2>About Us.</h2>

                <p>HanuSoft - Hanu programming club.</br>
                    Established in the early years of FIT faculty, by Mr. Trinh Bao Ngoc - Vice Dean of FIT. </br>
                    HNS is for programming enthusiasists, we create a motivational environment & help members pursue passionate programming - CORRECT & UPDATE.
                </p>
            </div>
        </div>

    </div>
</section>