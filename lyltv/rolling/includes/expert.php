<section id="testimonials" class="testimonial-section section-space-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-title">
                    <h2>Our experts</h2>
                    <p>There will be no teachers, we are bros, instructors work as guiders!</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="testimonial-carousel-list margin-top-20">
                <!--members-->
                <?php
                foreach ($users as $user) if ($user['role'] == 2){
                    ?>

                    <div class ="testimonial-word text-center">
                        <img src="<?php echo $user['avatar'];?>" class="img-responsive" alt="">
                        <h2><?php echo $user['name'] ?> </h2>
                        <p><?php echo $user['description'] ?></p>
                    </div>

                <?php } ?>
<!--
                            <div class="testimonial-word text-center">
                                <img src="images/testimonial/2.png" class="img-responsive" alt="">
                                <h2>John doe</h2>
                                <p>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duisauteiruredolor in reprehenderit in voluptate.</p>
                            </div>
                        -->
            </div>
        </div>
    </div>
</section>