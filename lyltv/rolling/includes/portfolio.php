<?php

$groups = getAllGroup($conn);
$users = getAllUser($conn);
?> 
<section id="portfolio" class="portfolio section-space-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-title">
                    <h2>Groups</h2>
                    <p>Dream teams from Hanusoft</p>
                </div>
            </div>
        </div>

        <div class="row">
            <ul class="portfolio">
                <li class="filter" data-filter="all">all</li>
                <?php foreach ($groups as $group): ?>  
                    <li class="filter" data-filter=" .<?php echo 'g'.$group['id'] ;?> "><?php echo $group['name'];?></li>
                <?php endforeach ?>
            </ul>
        </div>

        <div class="portfolio-inner">
            <div class="row">
                <?php foreach ($users as $user): ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 mix <?php $userId = $user['id'];
                    getGroupOfUser($userId, $conn);
                    ?>">
                    <div class = "container_overlay">
                        <a href="<?php echo $user['avatar']; ?>" width="300" height="316" class="portfolio-popup" title="<?php echo $user['description'];?>">
                            <img src="<?php echo $user['avatar']; ?>" alt="" width="500" height="316">
                            <div class="overlay">
                                <div class="text">
                                    <table class = "table">
                                      <tr style="margin-top: 5%;">
                                        <td >ID </td>
                                        <td><?php echo $user['id'].' '?></td>
                                    </tr>
                                    <tr>
                                        <td>Name </td>
                                        <td><?php echo $user['name']?></td>
                                    </tr>
                                    <tr>
                                        <td>Class </td>
                                        <td><?php echo $user['class']?></td>
                                    </tr>
                                    <tr>
                                        <td>Email </td>
                                        <td><?php echo $user['email']?></td>
                                    </tr>
                                    <tr>
                                        
                                        <td>Gender </td>
                                        <td><?php echo $user['gender']?></td>
                                    </tr><tr>
                                    <td>Descript </td>
                                    <td><?php echo $user['description']?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    <?php endforeach ?>
</div>
</div>
</div>

<div class="text-center margin-top-50">
    <a class="button button-style button-style-dark button-style-icon fa fa-long-arrow-right smoth-scroll" href="#contact">Let's Discuss</a>
</div>

</section>
