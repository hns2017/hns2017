<?php

$groups = getAllGroup($conn);
$users = getAllUser($conn);
?> 
<section id="portfolio" class="portfolio section-space-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-title">
                    <h2>Groups</h2>
                    <p>Dream teams from Hanusoft</p>
                </div>
            </div>
        </div>

        <div class="row">
            <ul class="portfolio">
                <li class="filter" data-filter="all">all</li>
                <li class="filter" data-filter=".g1"> Android Team</li>
                <li class="filter" data-filter=".g2"> PHP Team</li>
                <li class="filter" data-filter=".g2016"> HNS2016</li>
                <li class="filter" data-filter=".g2017"> HNS2017</li>
            </ul>
        </div>

        <div class="portfolio-inner">
            <div class="row">
                <?php foreach ($users as $user): if ($user['id'] <= 4) { ?>

                    <div class="col-md-4 col-sm-6 col-xs-12 mix g2016 g2017">
                        <div class="item">

                            <a href="" width="500" height="516" class="portfolio-popup" title="<?php echo $user['description'];?>">
                                <img src="<?php echo $user['avatar']; ?>" alt="" width="500" height="316">
                            </a>
                        </div>
                    </div>
                    <?php } endforeach ?>
                    <?php foreach ($users as $user): if ($user['id'] > 4) { ?>

                    <div class="col-md-4 col-sm-6 col-xs-12 mix g1 g2">
                        <div class="item">

                            <a href="<?php echo $user['avatar']; ?>" width="500" height="516" class="portfolio-popup" title="<?php echo $user['description'];?>">
                                <img src="<?php echo $user['avatar']; ?>" alt="" width="500" height="316">
                            </a>
                        </div>
                    </div>
                    <?php } endforeach ?>

                </div>
            </div>
        </div>

        <div class="text-center margin-top-50">
            <a class="button button-style button-style-dark button-style-icon fa fa-long-arrow-right smoth-scroll" href="#contact">Let's Discuss</a>
        </div>

    </section>