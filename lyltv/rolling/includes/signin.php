<?php
function validateUser($_POST){
	if (($_POST['pwd'] == $_POST['pwds']) && (!isIN($users,$_POST, $conn)))
		return true;
}
function isIN($users,$newUser, $conn){
	foreach ($users as $user) {
		if ($user['username'] == $username) ||
			($user['email'] == $newUser['email'])

		return true;
	}
}

function createUser($POST, $conn){
	$sql = "INSERT INTO USERS ('firstname','lastname','password','username','email','gender','avatar','class','description') VALUES ($_POST['firstname'],$_POST['lastname'],$_POST['password'],$_POST['username'],$_POST['email'],$_POST['gender'],$_POST['avatar'],$_POST['class'],$_POST['description'])";
	$result = $conn->query($sql);
}

function create($conn){
	$sql = "INSERT INTO USERS ('firstname','lastname','password','username','email','gender','avatar','class','description') VALUES ('1','2','123','Lily','lethivanly96@gmail.com',,,'1C','aa'";
	$result = $conn->query($sql);
}

function deleteUser(){}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Test</title>
	<!-- Meta Tag -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- SEO -->
	<meta name="description" content="150 words">
	<meta name="author" content="uipasta">
	<meta name="url" content="http://www.yourdomainname.com">
	<meta name="copyright" content="company name">
	<meta name="robots" content="index,follow">


	<title>HANUSOFT</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="images/favicon/logo_HANU.jpg">
	<link rel="apple-touch-icon" sizes="144x144" type="image/x-icon" href="images/favicon/hanusoft.jpg">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!-- All CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="css/plugin.css">

	<!-- Main CSS Stylesheet -->
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<!-- Google Web Fonts  -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">


	<!-- HTML5 shiv and Respond.js support IE8 or Older for HTML5 elements and media queries -->
    <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
       <![endif]-->

   </head>
   <body>
   	<section id="sign-up" class="sign-up-section section-space-padding bg-cover" data-stellar-background-ratio="0.3">
   		<div class="container">
   			<div class="row">
   				<div class="col-sm-12">
   					<div class="section-title">
   						<h2>Sign up</h2>
   						<p>Sign up to explore a great place to train yourself</p>
   					</div>
   					<form action = "signin.php">
   						<div>
   							<label for="email">Email</label>
   							<input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required>
   						</div>
   						<div>
   							<label for="pwd">Password</label>
   							<input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd" required>
   						</div>
   						<div>
   							<label for="pwd">Re-enter your password </label>
   							<input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwds" required>
   						</div>
   						<div>
   							<label for="firstname">Firstname</label>
   							<input type="firstname" class="form-control" id="firstname" placeholder="Enter Firstname" name="firstname" required>
   						</div>
   						<div>
   							<label for="lastname">Lastname</label>
   							<input type="lastname" class="form-control" id="lastname" placeholder="Enter Lastname" name="lastname" required>
   						</div>
   						<div>
   							<label for="username">Username</label>
   							<input type="username" class="form-control" id="username" placeholder="Enter Username to login next time" name="username" required>
   						</div>
   						<div>
   							<label for="pwd">Your gender</label>
   							<div class="radio">
   								<label><input type="radio" name="gender" value = "Male">Male</label>
   							</div>
   							<div class="radio">
   								<label><input type="radio" name="gender" value = "Female">Female</label>
   							</div>
   							<div class="radio">
   								<label><input type="radio" name="gender" value = "Other">Other</label>
   							</div>
   						</div>
   						<div>
   							<label for="comment">Tell us something about you: </label>
   							<textarea class="form-control" rows="5" id="comment"></textarea>
   						</div>
   						<div class="text-center margin-top-20">
   							<input id = "sign-up-button" type = "submit" value = "Sign Up">
   						</div>
   					</form>
   				</div>
   			</div>
   		</div>
   	</section>
   </body>
   </html>
