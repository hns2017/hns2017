<?php 
/**
 * returns user record specified by id or null if not exists
 */
function getUserByID($id, $conn) {
	$sql = "SELECT *, concat(firstname, ' ', lastname) as name FROM users WHERE id=$id";
	$result = $conn->query($sql);
	return $result->fetch();
}	

/**
 * returns array of user records
 * e.g: ...
 */
function getAllUser($conn) {
	$sql = "SELECT *, concat(firstname, ' ', lastname) as name FROM users";
	$result = $conn->query($sql);
	return $result->fetchAll();
}