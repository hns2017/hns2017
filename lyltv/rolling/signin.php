<?php 
include('includes/connect.php'); 
include('repositories/UserRepository.php');
include('repositories/GroupRepository.php');
include('repositories/SignUpRepository.php');
$users = getAllUser($conn);
session_start();
if ($_POST) {
    $_newUser = $_POST;
    if (validateNewUser($_newUser))
        createUser($_newUser, $conn);
    else {
        ?>
        <script type="text/javascript">
            alert("Sign up fail!")
        </script> 
        <?php }
    }
?>
<!DOCTYPE html>
<html>
<head>
	<!DOCTYPE html>
	<html lang="en">

	<head>

		<!-- Meta Tag -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- SEO -->
		<meta name="description" content="150 words">
		<meta name="author" content="uipasta">
		<meta name="url" content="http://www.yourdomainname.com">
		<meta name="copyright" content="company name">
		<meta name="robots" content="index,follow">


		<title>Sign in</title>

		<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon/logo_HANU.jpg">
		<link rel="apple-touch-icon" sizes="144x144" type="image/x-icon" href="images/favicon/hanusoft.jpg">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!-- All CSS Plugins -->
		<link rel="stylesheet" type="text/css" href="css/plugin.css">

		<!-- Main CSS Stylesheet -->
		<link rel="stylesheet" type="text/css" href="css/style.css">

		<!-- Google Web Fonts  -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">


		<!-- HTML5 shiv and Respond.js support IE8 or Older for HTML5 elements and media queries -->
    <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
       <![endif]-->

    </head>
    <body>

       <div class="header-top-area" style="background-color: black">
        <div class="container">
         <div class="row">

          <div class="col-sm-3">
           <div class="logo">
            <a href="http://fit.hanu.vn/">FIT</a>
         </div>
      </div>

      <div class="col-sm-9">
        <div class="navigation-menu">
         <div class="navbar">
          <div class="navbar-header">
           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
         <li class="active"><a class="smoth-scroll" href="index.php#home">Home <div class="ripple-wrapper"></div></a>
         </li>
         <li><a class="smoth-scroll" href="index.php#about">About</a>
         </li>
         <li><a class="smoth-scroll" href="index.php#portfolio">Groups</a>
         </li>
         <li><a class="smoth-scroll" href="index.php#testimonials">Experts</a>
         </li>
         <li><a class="smoth-scroll" href="index.php#services">Services</a>
         </li>
         <li><a class="smoth-scroll" href="index.php#contact">Contact</a>
         </li>
         <li><a class="smoth-scroll" href="signin.php">Sign in</a>
         </li>
         <li><a class="smoth-scroll" href="index.php#signup.php">Sign up</a>
         </li>
      </ul>
   </div>
</div>
</div>
</div>
</div>
</div>
</div>

<section id="sign-in" class="sign-in section-space-padding bg-cover" data-stellar-background-ratio="0.3">
   <div class="section-title"><h2>Sign in</h2></div>
   <form action = "index.php" method="POST">
      <div class="row">
         <div class="col-sm-4">
         </div>
         <div class="col-sm-4">
            <div>
               <label for="email">Email</label>
               <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required>
            </div>
            <div>
               <label for="password">Password</label>
               <input type="password" class="form-control" id="password" placeholder="Enter password" name="password" required>
            </div>                     
         </div>
      </div>
      <div class="text-center margin-top-20">
         <button class="btn btn-primary" type = "submit" value = "Sign in">Sign in</button>
      </div>
   </form>
</section>