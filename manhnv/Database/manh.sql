
/* creating table users*/
CREATE TABLE users(
    id int NOT NULL AUTO_INCREMENT,
    username varchar(50) NOT NULL,
    password varchar(61) NOT NULL,
	role int(1) NOT NULL,
	firstname varchar(255),
	lastname varchar(255),
	email varchar(255),
	gender enum('male','female','null'),
	avatar varchar(255),
	dob date,
	description MEDIUMTEXT,
	PRIMARY KEY (id)
);
/* insertting information of table users*/
INSERT INTO users 
    (firstname,lastname,gender,dob)
VALUES
    ('Ngoc','Tran','Male','1980-5-20'),
	('Cong','Nguyen','Male','1992-5-20'),
	('Trung','Dao','Male','1997-5-20'),
	('Manh','Nguyen','Male','1997-11-21'),
	('Binh','DC','Male','1992-5-20'),
	('Nam','Nguyen','Male','1996-5-20'),
	('Duy','Nguyen','Male','1996-5-20'),
	('Ly','Van','Female','1996-5-20'),
	('Nguyet','Nguyen','Female','1996-8-1'),
	('Tuyet','Nguyen','Female','1996-12-1'),
	('Duc','Nguyen','male','1996-5-15'),
	('Thanh','Ly','male','1996-8-23'),
	('Thinh','Nguyen','male','1996-9-5'),
	('Quyen','Nguyen','male','1996-4-15'),
	('Doanh','Nguyen','male','1996-8-5'),
	('Hao','Nguyen','male','1996-9-7'),
	('Thuong','Nguyen','Female','1996-10-27'),
	('Hang','Nguyen','Female','1996-11-14'),
	('Hue','Hoang','Female','1996-3-23'),
	('Hung','Nguyen','male','1996-2-14'),
	('Tuong','Nguyen','male','1996-1-25'),
	('Thuc','Nguyen','male','1996-7-26'),
	('Toan','Nguyen','male','1996-9-7'),
	('Hai','Nguyen','male','1996-3-4'),
	('Huong','Nguyen','Female','1996-8-18'),
	('Trang','Nguyen','Female','1996-5-19'),
	('Cong','Nguyen','male','1996-6-15'),
	('Hoi','Nguyen','male','1996-7-1'),
	('Cuc','Hoang','Female','1997-12-22'),
	('Tuan','Nguyen','Male','1985-8-2'),
	('Long','Nguyen','Male','1987-7-20'),
	('Bao','Ngo','Male','1997-6-2');
	
/* list all members. member's name = first name + last name*/	
SELECT firstname,lastname,gender,dob FROM users;
/* list top 3 most active members. */
SELECT users.*, COUNT(userproject.id) AS no_projects 
 FROM users
 INNER JOIN userproject ON users.id = userproject.userid
 GROUP BY users.id
 ORDER BY no_projects DESC
 LIMIT 3;
/* list first 5 members in k14 order ascending by members' name*/
SELECT  firstname,lastname,gender,dob FROM users
WHERE dob BETWEEN '1996-1-1' AND '1996-12-31'
ORDER BY firstname ASC
LIMIT 5;
/*  list next 5 members in k14 order ascending by members' name*/
SELECT  firstname,lastname,gender,dob FROM users
WHERE dob BETWEEN '1996-1-1' AND '1996-12-31'
ORDER BY firstname ASC
LIMIT 5, 5;
/*use boolean as datatype for Member.gender, 1 = male, 0 = female*/
select id,username,password,
case gender
   when 'Male' then 'Nam' 
   when 'Female' then 'Nu'
end
from users;



/* create table userproject*/
CREATE TABLE userproject (
    id int NOT NULL AUTO_INCREMENT,
    userid int NOT NULL,
    projectid int NOT NULL,
    PRIMARY KEY (id)
);
/*insert infor  into table userproject*/
INSERT INTO userproject 
    (userid,projectid)
VALUES
    ('2','1'),
	('2','2'),
	('3','3'),
	('3','4');
	('4','5'),
	('5','6');
/* create table projects*/
CREATE TABLE projects (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    description MEDIUMTEXT,
    sourcecode varchar(255),
    status enum('completed','on-going','pending') NOT NULL,
	created_at date NOT NULL,
	PRIMARY KEY (id)
);

/* insert infor into projects*/
INSERT INTO projects 
    (name,status,created_at)
VALUES
    ('Hanusoft','on-going','2017-12-5'),
	('FitHanu','completed','2006-6-12'),
	('Hanu.vn','completed','2000-2-2'),
	('KnightAge','on-going','2016-2-3'),
	('It.net','on-going','2017-1-6'),
	('HocIT.vn','pending','2018-1-20');

	/*list all projects, latest -to- oldest ones.*/
SELECT * FROM projects 
ORDER BY created_at ASC;
/*ist all projects are on-going*/
SELECT * FROM projects 
WHERE status='on-going';
/* list all members joined in project HanuSoft website*/
 SELECT *
 FROM users
 INNER JOIN userproject ON users.id = userproject.id
 WHERE userproject.projectid =1;
 /* list all projects are implemented by Cong*/
 SELECT *
 FROM projects
 INNER JOIN userproject ON projects.id = userproject.id
 WHERE userproject.userid =2;
 /*  assign Cong to project HanuSoft website*/
INSERT INTO userproject 
    (userid,projectid)
VALUES
    ('2','1');
 /* create table Team*/
CREATE TABLE team (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    description varchar(255) NOT NULL,
    PRIMARY KEY (id)
);
/* insert information in table team*/
INSERT INTO team 
    (name,description)
VALUES
    ('Yii','Say yes'),
	('Tii','Say yes'),
	('Gii','Say no'),
	('Kii','Never say no'),
	('Hii','Say yes');
/*list all members of Yii team */
 SELECT *
 FROM users
 INNER JOIN team ON users.id = team.id
 WHERE team.id =1;
 /* list all teams (groups) those Cong joined in**/
 SELECT *
 FROM team
 INNER JOIN userteam ON team.id = userteam.id
 WHERE userteam.userteamid =2;
 /*fire Cong out of Gii team*/
DELETE FROM userteam
WHERE userteamid = 2;
/*report the number of joined in - projects for each member in 2017*/
SELECT users.*,projects.* FROM users 
INNER JOIN  userproject  ON   users.id= userproject.userid
INNER JOIN  projects  ON   projects.id= userproject.projectid
WHERE projects.created_at LIKE '%2017%'
ORDER BY users.firstname ASC;
/* create  table userteam*/
CREATE TABLE userteam (
    id int NOT NULL AUTO_INCREMENT,
    teamid int NOT NULL,
	userteamid int NOT NULL,
    PRIMARY KEY (id)
);
/* insert  infor into username*/
INSERT INTO userteam 
    (teamid,userteamid)
VALUES
    ('1','1'),
	('2','1'),
	('3','2'),
	('4','2'),
	('5','3'),
	('2','3'),
	('2','4'),
	('4','4'),
	('4','5'),
	('5','5'),
	('3','6'),
	('2','6'),
	('3','7'),
	('4','7'),
	('5','8');